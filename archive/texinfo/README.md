# TeXinfo och Info-version av Svenska jargongfilen

Senast rörd 2003. Ursprungligen automatkonverterade från
LinuxDoc-DTD:n och sedan manuellt redigerad.

Källkoden finns i [hackerswe.texi](hackerswe.texi) och externa länkar
som är definierade i [hackextlin.texi](hackextlin.texi).

De renderade versionerna i HTML och Info-format är skapade av GNU
makeinfo.
