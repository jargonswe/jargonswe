TARGETS = jargonswe.html

all: $(TARGETS)

.SUFFIXES: .html .md

.md.html:
	pandoc -f markdown-smart -t html5 $< -o $@ --css=style.css --self-contained

clean:
	rm $(TARGETS)
